package com.ldi;

import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import com.datastax.oss.driver.api.core.metadata.schema.ClusteringOrder;
import com.datastax.oss.driver.api.core.type.DataTypes;
import com.datastax.oss.driver.api.querybuilder.SchemaBuilder;
import com.datastax.oss.driver.api.querybuilder.schema.CreateKeyspace;
import com.datastax.oss.driver.api.querybuilder.schema.CreateTable;
import com.datastax.oss.driver.api.querybuilder.schema.CreateTableStart;
import com.datastax.oss.driver.api.querybuilder.schema.CreateTableWithOptions;

import static com.datastax.oss.driver.api.querybuilder.SchemaBuilder.*;


/**
 * Create a table in Cassandra
 */

public class CasCreateTable {

    public static void main(String[] args) {

        try (CqlSession session = CqlSession.builder().build()) {
            CreateKeyspace createKs = createKeyspace("cycling").withSimpleStrategy(1);
            session.execute(createKs.build());

            CreateTable createTable =
                    createTable("cycling", "cyclist_name")
                            .withPartitionKey("id", DataTypes.UUID)
                            .withColumn("lastname", DataTypes.TEXT)
                            .withColumn("firstname", DataTypes.TEXT);

            session.execute(createTable.build());
            System.out.println("Table created successfully");
        }

    }


}
