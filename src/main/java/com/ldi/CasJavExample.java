package com.ldi;
import com.datastax.oss.driver.api.core.CqlSession;
import com.datastax.oss.driver.api.core.cql.ResultSet;
import static com.datastax.oss.driver.api.core.CqlSession.builder;

/**
 * Example of java class connecting to C*
 */

public class CasJavExample {
    public static void main(String[] args) {
        try (CqlSession session = builder().build()) {
            ResultSet rs = session.execute("SELECT release_version FROM system.local");
            System.out.println(rs.one().getString(0));


        }
    }

}
